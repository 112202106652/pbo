/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author ASUS
 */
public class Mhs 
{
    //property...........................
    private String nama;
    private float ipk;
    private String nim;
    private String prodi;
    private String kodeMakul;
    
    //behavior...........................
    public void setNama(String nama)
    {
        this.nama = nama;
    }
    
    public void setIpk(int ipk)
    {
        this.ipk = ipk;
    }
    
    public String getNama() {
        return this.nama;
    }
    
    public float getIpk() {
        return this.ipk;
    }
    public String getNim() {
        return nim;
    }
    public void setNim(String nim) {
        this.nim = nim;
    }

    public String getProdi() {
        return prodi;
    }

    public void setProdi(String prodi) {
        this.prodi = prodi;
    }

    public String getKodeMakul() {
        return kodeMakul;
    }

    public void setKodeMakul(String kodeMakul) {
        this.kodeMakul = kodeMakul;
    }
 }

