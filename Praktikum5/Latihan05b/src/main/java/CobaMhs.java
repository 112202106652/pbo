/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author ASUS
 */
public class CobaMhs 
{
    public static void main(String[] args)
    {
        Mhs mhsFadlil = new Mhs();
        
        mhsFadlil.setNama("Fadlil Chandra");
        mhsFadlil.setNim("A12.2021.06652");
        mhsFadlil.setProdi("Sistem Informasi");
        mhsFadlil.setKodeMakul("A12.66403");
        mhsFadlil.setIpk(4);
        
        System.out.println("Nama Mhs\t\t: " + mhsFadlil.getNama());
        System.out.println("NIM Mhs\t\t\t: " + mhsFadlil.getNim());
        System.out.println("Program Studi Mhs\t: " + mhsFadlil.getProdi());
        System.out.println("Kode Makul Mhs\t\t: " + mhsFadlil.getKodeMakul());
        System.out.println("IPK Mhs\t\t\t: " + mhsFadlil.getIpk());
    }
}
