/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.latihan04;

/**
 *
 * @author ASUS
 */
import java.util.Scanner;
public class Latihan04c 
{
    public static void main(String[] args) {
        Scanner inp =  new Scanner(System.in);
        NilaiMahasiswa nilai = new NilaiMahasiswa ();
        System.out.println("Data Test");
        System.out.println("=====================");
        System.out.print("Nama \t\t: ");
        nilai.nama = inp.nextLine ();
        System.out.print("Programstudi \t: ");
        nilai.programstudi = inp.nextLine ();
        System.out.print("Nilai \t\t: ");
        nilai.nilai = inp.nextInt ();
        System.out.println("Nilai huruf \t: " + nilai.kategori());
        System.out.println("=====================");
    }
}    
