/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.latihan04;


/**
 *
 * @author ASUS
 */
import java.util.Scanner;
public class Latihan04d 
{
    public static void main(String[] args) {
        Scanner inp =  new Scanner(System.in);
        PembelianAir beli = new PembelianAir();
        System.out.println("Perhitunhgan Biaya Pemakaian Air");
        System.out.println("======================================");
        System.out.print("Nama  \t\t : ");
        beli.nama = inp.nextLine();
        System.out.print("No. Pelanggan \t : ");
        beli.np = inp.nextLine();
        System.out.print("Pemakaian Air \t : ");
        beli.pakai = inp.nextInt();
        
        System.out.println("Biaya Pakai \t : " + beli.biaya());
        System.out.println("======================================");
    }
}
