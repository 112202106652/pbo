/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package impllementasi_smartphone;

/**
 *
 * @author Administrator
 */
public interface Smartphone 
{
   public static final int MAX_VOLUME = 100;
    public static final int MIN_VOLUME = 0;
    public static final int MAX_BATT_LEVEL = 100;
    public static final int MIN_BATT_LEVEL = 0;
   
   void topUpCredit(int credit);
   void checkCreditBalance();
   void viewAllContacts();
   public void addContact(String no, String nama);
   public void searchContacts(String nama);
}
