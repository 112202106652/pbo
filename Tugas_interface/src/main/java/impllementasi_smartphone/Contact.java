/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package impllementasi_smartphone;

/**
 *
 * @author Administrator
 */
public class Contact {
    String nama;
    String nomor;

    public Contact(String nama, String nomor)
    {
        this.nama = nama;
        this.nomor = nomor;
    }

    String getNama()
    {
        return this.nama;
    }

    String getNomor()
    {
        return this.nomor;
    }  
}
