/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package impllementasi_smartphone;

/**
 *
 * @author Administrator
 */
public class CellphoneMain {
    public static void main(String[] args){
        Cellphone cp = new Cellphone("Sonny Ericsson", "W2000i");
        
        cp.powerOn();
        
        cp.topUpCredit(10000);
        cp.addContact("0810794389", "Peter");
        cp.addContact("081234567890", "Dasha");
        
        cp.viewAllContacts();
        
        cp.searchContacts("Peter");
        
        cp.powerOff();
    }    
}
