/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author HP
 */
public class Data {
    static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://127.0.0.1/bioskop";
    static final String USER = "root";
    static final String PASS = "";
    static Connection conn;
    static Statement stmt;
    static ResultSet rs;
    static PreparedStatement ps;
    
    public DefaultTableModel showtable(){
        try{
            Class.forName(JDBC_DRIVER);	   
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            DefaultTableModel model = new DefaultTableModel();
            model.addColumn("ID");
            model.addColumn("Username");
            model.addColumn("Nama Film");
            model.addColumn("Total Kursi");
            model.addColumn("Kursi");
            model.addColumn("Harga");
 
            stmt = conn.createStatement();
            String sql = "SELECT transaksi_pemesanan.id_pemesanan AS id, user.username AS username, jadwal_film.nama AS 'nama film', (SELECT COUNT(*) FROM transaksi_pemesanan, detail_pemesanan WHERE detail_pemesanan.id_pemesanan = transaksi_pemesanan.id_pemesanan) AS `total kursi`, (SELECT GROUP_CONCAT(CONCAT('Kursi : ', detail_pemesanan.nomor_kursi, ' Waktu : ', jam_tayang.jam_tayang) ORDER BY detail_pemesanan.nomor_kursi ASC SEPARATOR '\n') FROM transaksi_pemesanan, detail_pemesanan, jam_tayang WHERE detail_pemesanan.id_pemesanan = transaksi_pemesanan.id_pemesanan and detail_pemesanan.id_jam_tayang = jam_tayang.id_jam_tayang ) AS kursi, transaksi_pemesanan.harga AS harga FROM transaksi_pemesanan , user , jadwal_film";
            int i = 1;
            rs = stmt.executeQuery(sql);
            while(rs.next()) {
            	model.addRow(new Object[] {
                        i,
            		rs.getString("id"),
            		rs.getString("username"),
            		rs.getString("nama film"),
            		rs.getString("total kursi"),
                        rs.getString("kursi"),
                        rs.getString("harga")
            	});
            	i++;
            }
            rs.close();
            conn.close();
            stmt.close();
            return model;
        }
        
        
        catch(ClassNotFoundException e){
            e.printStackTrace();
            return null;
        }
        catch(SQLException e){
            e.printStackTrace();
            return null;
        }
    }
    
    public DefaultComboBoxModel showFilm(){
        try{
            Class.forName(JDBC_DRIVER);	   
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
//            ArrayList model = new ArrayList<String>();
            DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>();
            model.addElement("--Pilih Film--");
 
            stmt = conn.createStatement();
            String sql = "SELECT nama FROM jadwal_film";
            rs = stmt.executeQuery(sql);
            while(rs.next()) {
                model.addElement(rs.getString("nama"));
            }
            rs.close();
            conn.close();
            stmt.close();
            return model;
        }
        
        
        catch(ClassNotFoundException e){
            e.printStackTrace();
            return null;
        }
        catch(SQLException e){
            e.printStackTrace();
            return null;
        }
    }
    
    public DefaultComboBoxModel showJamTayang(String namaFilm){
        try{
            Class.forName(JDBC_DRIVER);	   
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
//            ArrayList model = new ArrayList<String>();
            DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>();
            model.addElement("--Pilih Jam Tayang--");
            
            if(namaFilm == null){
                namaFilm = "Fast and Furious 6";
            }
            
            stmt = conn.createStatement();
            String sql = "SELECT id_film FROM jadwal_film WHERE nama = '" + namaFilm + "'";
            rs = stmt.executeQuery(sql);
            System.out.println(rs.getString(1));
//            String id = rs.getObject(0);
//            sql = "SELECT jam_tayang.jam_tayang as 'jam tayang' FROM jam_tayang, jadwal_film WHERE jam_tayang.id_film = '" + id + "'";
            rs = stmt.executeQuery(sql);
            while(rs.next()) {
                model.addElement(rs.getString(1));
            }
            rs.close();
            conn.close();
            stmt.close();
            return model;
        }
        
        
        catch(ClassNotFoundException e){
            e.printStackTrace();
            return null;
        }
        catch(SQLException e){
            e.printStackTrace();
            return null;
        }
    }
}
